import { UpdateServingsUseCase } from "../src/usecases/update-servings.usecase";
import { Detail } from "../src/model/detail";

describe("update servings use case", () => {
  it("should update the recipe servings and ingredients", () => {
    const RECIPE = new Detail({
      id: "5ed6604591c37cdc054bc886",
      title: "Spicy Chicken and Pepper Jack Pizza",
      publisher: "My Baking Addiction",
      sourceurl:
        "http://www.mybakingaddiction.com/spicy-chicken-and-pepper-jack-pizza-recipe/",
      image: "http://forkify-api.herokuapp.com/images/FlatBread21of1a180.jpg",
      servings: 4,
      cookingTime: 45,
      ingredients: [
        {
          quantity: 1,
          unit: "",
          description: "tbsp. canola or olive oil",
        },
        {
          quantity: 0.4,
          unit: "cup",
          description: "chopped sweet onion",
        },
        {
          quantity: 4,
          unit: "cups",
          description: "diced fresh red yellow and green bell peppers",
        },
      ],
    });

    const newServings = 5;
    const updatedRecipe = UpdateServingsUseCase.execute(RECIPE, newServings);

    expect(updatedRecipe.servings).toBe(newServings);
    expect(updatedRecipe.ingredients[0].quantity).toBe(1.25);
    expect(updatedRecipe.ingredients[1].quantity).toBe(0.5);
    expect(updatedRecipe.ingredients[2].quantity).toBe(5);
  });

  it("should not update the recipe if servings is < 1", () => {
    const RECIPE = new Detail({
      id: "5ed6604591c37cdc054bc886",
      title: "Spicy Chicken and Pepper Jack Pizza",
      publisher: "My Baking Addiction",
      sourceurl:
        "http://www.mybakingaddiction.com/spicy-chicken-and-pepper-jack-pizza-recipe/",
      image: "http://forkify-api.herokuapp.com/images/FlatBread21of1a180.jpg",
      servings: 4,
      cookingTime: 45,
      ingredients: [
        {
          quantity: 1,
          unit: "",
          description: "tbsp. canola or olive oil",
        },
        {
          quantity: 0.4,
          unit: "cup",
          description: "chopped sweet onion",
        },
        {
          quantity: 4,
          unit: "cups",
          description: "diced fresh red yellow and green bell peppers",
        },
      ],
    });

    const newServings = 0;
    const updatedRecipe = UpdateServingsUseCase.execute(RECIPE, newServings);

    expect(updatedRecipe).toBe(RECIPE);
  });
});
