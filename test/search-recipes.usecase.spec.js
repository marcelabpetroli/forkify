import { RecipesRepository } from "../src/repositories/recipes.repository";
import { SearchRecipesUseCase } from "../src/usecases/search-recipes.usecase";
import { RECIPES } from "./fixtures/recipes";

jest.mock("../src/repositories/recipes.repository");

describe("search recipes use case", () => {
  beforeEach(() => {
    RecipesRepository.mockClear();
  });

  it("should get recipes that match query", async () => {
    RecipesRepository.mockImplementation(() => {
      return {
        searchRecipes: (query) => {
          return {
            data: {
              recipes: RECIPES.filter((recipe) =>
                recipe.title.toLowerCase().includes(query.toLowerCase())
              ),
            },
          };
        },
      };
    });

    const query = "pita";
    const foundRecipe = await SearchRecipesUseCase.execute(query);

    expect(foundRecipe.length).toBe(5);
    expect(foundRecipe[0].title).toBe("Crispy Chickpea Pita");
    expect(foundRecipe[0].publisher).toBe("Real Simple");
    expect(foundRecipe[0].id).toBe("5ed6604591c37cdc054bcbbf");
  });
});
