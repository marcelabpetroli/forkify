import { LoadRecipeUseCase } from "../src/usecases/load-recipe.usecase";
import { RecipesRepository } from "../src/repositories/recipes.repository";

jest.mock("../src/repositories/recipes.repository");

describe("load recipe use case", () => {
  beforeEach(() => {
    RecipesRepository.mockClear();
  });
  it("should load the clicked recipe", async () => {
    RecipesRepository.mockImplementation(() => {
      return {
        loadRecipe: (id) => {
          return {
            data: {
              recipe: {
                publisher: "PBS Food",
                ingredients: [Array],
                source_url:
                  "http://www.pbs.org/food/recipes/middle-eastern-stuffed-pitas/",
                image_url:
                  "http://forkify-api.herokuapp.com/images/13124891871ac7.908722_large288x162.jpg",
                title: "Middle Eastern Stuffed Pitas",
                servings: 4,
                cooking_time: 60,
                id: id,
              },
            },
          };
        },
      };
    });

    const id = "5ed6604591c37cdc054bc87f";
    const recipe = await LoadRecipeUseCase.execute(id);

    expect(recipe.id).toBe("5ed6604591c37cdc054bc87f");
    expect(recipe.title).toBe("Middle Eastern Stuffed Pitas");
    expect(recipe.publisher).toBe("PBS Food");
  });
});
