export const RECIPES = [
  {
    publisher: "Real Simple",
    image_url:
      "http://forkify-api.herokuapp.com/images/chickenpita_3000acbd577.jpg",
    title: "Crispy Chickpea Pita",
    id: "5ed6604591c37cdc054bcbbf",
  },
  {
    publisher: "Epicurious",
    image_url: "http://forkify-api.herokuapp.com/images/3538154d4a.jpg",
    title: "Avocado and Grapefruit Salad",
    id: "5ed6604691c37cdc054bd093",
  },
  {
    publisher: "Two Peas and Their Pod",
    image_url:
      "http://forkify-api.herokuapp.com/images/AvocadoEggSalad96660.jpg",
    title: "Avocado Egg Salad",
    id: "5ed6604591c37cdc054bce86",
  },
  {
    publisher: "Closet Cooking",
    image_url:
      "http://forkify-api.herokuapp.com/images/BBQChickenPizzawithCauliflowerCrust5004699695624ce.jpg",
    title: "Cauliflower Pizza Crust (with BBQ Chicken Pizza)",
    id: "5ed6604591c37cdc054bcd09",
  },
  {
    publisher: "Closet Cooking",
    image_url:
      "http://forkify-api.herokuapp.com/images/BBQChickenPizzawithCauliflowerCrust5004699695624ce.jpg",
    title: "Cauliflower Pizza Crust (with BBQ Chicken Pizza)",
    id: "5ed6604591c37cdc054bcc13",
  },
  {
    publisher: "Simply Recipes",
    image_url:
      "http://forkify-api.herokuapp.com/images/hummus300x2008521b280.jpg",
    title: "Hummus",
    id: "5ed6604591c37cdc054bcb3a",
  },
  {
    publisher: "Epicurious",
    image_url:
      "http://forkify-api.herokuapp.com/images/epicuriousfacebook511b.png",
    title: "Falafel with Hummus",
    id: "5ed6604691c37cdc054bd02f",
  },
  {
    publisher: "Simply Recipes",
    image_url:
      "http://forkify-api.herokuapp.com/images/pizza292x2007a259a79.jpg",
    title: "Homemade Pizza",
    id: "5ed6604591c37cdc054bcb34",
  },
  {
    publisher: "Simply Recipes",
    image_url:
      "http://forkify-api.herokuapp.com/images/howtogrillpizzad300x20086a60e1b.jpg",
    title: "How to Grill Pizza",
    id: "5ed6604591c37cdc054bcb37",
  },
  {
    publisher: "Closet Cooking",
    image_url:
      "http://forkify-api.herokuapp.com/images/Pizza2BDip2B12B500c4c0a26c.jpg",
    title: "Pizza Dip",
    id: "5ed6604591c37cdc054bcac4",
  },
  {
    publisher: "Real Simple",
    image_url:
      "http://forkify-api.herokuapp.com/images/tenbeefpitas_3005f95959e.jpg",
    title: "Mediterranean Beef Pitas",
    id: "5ed6604591c37cdc054bcba3",
  },
  {
    publisher: "Two Peas and Their Pod",
    image_url:
      "http://forkify-api.herokuapp.com/images/minifruitpizzas52c00.jpg",
    title: "Mini Fruit Pizzas",
    id: "5ed6604591c37cdc054bce0d",
  },
  {
    publisher: "Two Peas and Their Pod",
    image_url:
      "http://forkify-api.herokuapp.com/images/CobbSaladSandwich5c185.jpg",
    title: "Cobb Salad Sandwich",
    id: "5ed6604591c37cdc054bcfbb",
  },
  {
    publisher: "Chow",
    image_url:
      "http://forkify-api.herokuapp.com/images/30428_RecipeImage_620x413_cuban_sandwichdb56.jpg",
    title: "Cuban Sandwiches Recipe",
    id: "5ed6604691c37cdc054bd00d",
  },
  {
    publisher: "Bon Appetit",
    image_url:
      "http://forkify-api.herokuapp.com/images/nokneadpizzadoughlahey6461467.jpg",
    title: "No-Knead Pizza Dough",
    id: "5ed6604591c37cdc054bcd86",
  },
  {
    publisher: "PBS Food",
    image_url:
      "http://forkify-api.herokuapp.com/images/13124891871ac7.908722_large288x162.jpg",
    title: "Middle Eastern Stuffed Pitas",
    id: "5ed6604591c37cdc054bc87f",
  },
  {
    publisher: "Naturally Ella",
    image_url:
      "http://forkify-api.herokuapp.com/images/IMG_7147200x30017a6.jpg",
    title: "Grilled Beet and Hummus Stuffed Pita",
    id: "5ed6604691c37cdc054bd009",
  },
  {
    publisher: "Two Peas and Their Pod",
    image_url:
      "http://forkify-api.herokuapp.com/images/avocadopizzawithcilantrosauce4bf5.jpg",
    title: "Avocado Pita Pizza with Cilantro Sauce",
    id: "5ed6604591c37cdc054bce0f",
  },
];
