# Forkify

Forkify is a web application that allows users to find thousands of recipes and adjust the servings according to their needs. It is a Single Page Application built with Lit Element and Webpack.

## Features

- Browse and search for a wide variety of recipes.
- Adjust the servings of recipes to suit user requirements.
- User-friendly interface for easy navigation and interaction.

## Technologies Used

- `Lit Element`: A lightweight web component library for building reusable user interface components.
- `Webpack`: A module bundler used for packaging the application's assets and dependencies.
- `Jest`: A JavaScript testing framework used for unit testing the project.
- `Cypress`: An end-to-end testing framework used for automated testing of the application's functionality.
- `Sass`: A CSS preprocessor used for styling the web application.
- `API`: The application connects to an API to fetch recipe data. You can access the API [here](https://forkify-api.herokuapp.com/v2).
- `Routing`: The application routing is handled by Vaadin Router library.

# ☝️ Getting Started

To get started with Forkify, follow these steps:

1. Clone the repository:

```console
git clone git@gitlab.com:marcelabpetroli/forkify.git
```

2. Install the dependencies:

```console
npm install
```

3. Start the project:

```console
npm start
```

4. Check the shell, the project will be initialized:

```console
http://localhost:8080
```

5. Run unit tests with Jest using:

```console
npm test
```

6. Run e2e tests with Cypress using:

```console
npm run cy:open
```

# 🧪 Testing

The project was created following `Test-Driven-Development (TDD)` principles to ensure code quality.

- Unit tests with Jest for automation, refactoring and code maintainability.
- End-to-end tests with Cypress for realistic simulation of user interaction with the app, comprehensive test coverage, debugging and reliability.

# 🐳 Docker

The project includes a Dockerfile for containerization. Use the following steps to build and run the Docker container:

1. Run the Docker container:

```console
docker run -d -p "8080:80" marcelabpe/forkify:1.0.0
```

2. Additionally, you can run with `docker-compose`:

```console
docker-compose up -d
```

# 💬 Conventional Commit Convention

The project uses 🐾 `Husky` to enforce the conventional commit convention for commit messages. This convention ensures consistent and descriptive commit messages. This is possible using two files:

- pre-commit: run all tests when a commit is made.
- commit-msg: validates commit message according to conventional commits standards.

# ⚙️ Continuous Integration and Deployment

The project includes a pipeline with multiple stages for continuous integration and deployment. The stages are as follows:

- Build: Builds the project and generates the necessary artifacts.
- Cypress: Runs end-to-end tests using Cypress.
- Deploy: Builds a Docker image and pushes it to a Docker registry.
- Sonar: Performs static code analysis using SonarCloud.
- Hosting: Deploys the application using Firebase hosting.

Please note that the deployment and hosting stages may require additional configuration and environment variables.

# ✨ Credits

- Forkify is a copyright of the brilliant developer Jonas Schmedtmann. He created the project for his JavaScript course on the Udemy platform.
- His implementation uses JavaScript, Sass and Parcel as the bundler.
- All `use cases, tests, continuous integration and continuous deployment functionalities` were created and implemented by Marcela.

# 🤩 The App

You can visit the webpage [here](https://forkify-6f5ac.web.app/).

I hope you have fun using the app and find cool recipes to cook at home!

Cheers!
