describe("template spec", () => {
  it("should find welcome message", () => {
    cy.visit("/");
    cy.get(".home__title").contains(
      "Welcome to Forkify! Ready to get inspired?"
    );
  });

  beforeEach(() => {
    cy.visit("/");
    cy.get(".go__link").click();
  });

  it("should go back to home when pressing back to home button", () => {
    cy.get(".link").click();
    cy.get(".home__title").contains(
      "Welcome to Forkify! Ready to get inspired?"
    );
  });

  it("should find default message when no results", () => {
    cy.get(".message > p").contains(
      "Use the search bar to find thousands of inspiring recipes!"
    );
  });

  it("should find no results message when query does not match", () => {
    cy.get(".search__field").type("WWW");
    cy.get(".btn").click();
    cy.get("#recipe__list > .message").contains(
      "There are no recipes that match your search"
    );
  });

  it("should type the recipe, press search and display recipe", () => {
    cy.get(".search__field").type("pita");
    cy.get(".btn").click();
    cy.get("#recipe__list > .results").children().should("have.length", 5);
    cy.get(
      "#recipe__list > .results > #5ed6604591c37cdc054bcbbf > recipe-ui > .preview__link > .preview__data > .preview__title"
    ).contains("Pita");
    cy.get(
      "#recipe__list > .results > #5ed6604591c37cdc054bcbbf > recipe-ui > a"
    ).click();
    cy.get(".recipe__box").should("exist");
    cy.get(".recipe__details").should("exist");
    cy.get(".recipe__ingredients").should("exist");
  });

  it("should type the recipe, press search and display recipe with pagination", () => {
    cy.get(".search__field").type("pizza");
    cy.get(".btn").click();
    cy.get("#recipe__list > .results").children().should("have.length", 10);
    cy.get(".page__btn").contains("Page 2");
    cy.get(".page__btn").click();
    cy.get(".btn--prev").should("exist");
    cy.get(".btn--next").should("exist");
  });

  it("should search for recipe, go to next page, and do a new search and start from page 1", () => {
    cy.get(".search__field").type("pizza");
    cy.get(".btn").click();
    cy.get(".page__btn").contains("Page 2");
    cy.get(".btn--next").click();
    cy.get(".search__field").clear();
    cy.get(".search__field").type("pita");
    cy.get(".btn").click();
    cy.get(".btn--prev").should("not.exist");
    cy.get(".btn--next").should("not.exist");
  });
});
