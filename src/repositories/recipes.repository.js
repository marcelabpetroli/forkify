import axios from "axios";

export class RecipesRepository {
  async searchRecipes(query) {
    return await (
      await axios.get(
        `https://forkify-api.herokuapp.com/api/v2/recipes/?search=${query}`
      )
    ).data;
  }

  async loadRecipe(id) {
    return await (
      await axios.get(`https://forkify-api.herokuapp.com/api/v2/recipes/${id}`)
    ).data;
  }
}
