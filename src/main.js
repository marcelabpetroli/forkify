import "./sass/main.scss";
import "./pages/home.page";
import "./pages/recipes.page";
import "./components/recipe-search.component";
import "./components/recipe-list.component";
import "./components/recipe-detail.component";
import "./ui/ingredients.ui";
import "./ui/pagination.ui";
import "./ui/header.ui";
import "./ui/recipe.ui";
import "./ui/copy.ui";
import "./ui/home-ui";

import { Router } from "@vaadin/router";
const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/recipes", component: "recipes-page" },
  { path: "(.*)", redirect: "/" },
]);
