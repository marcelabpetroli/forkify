export class Detail {
  constructor({
    id,
    title,
    publisher,
    sourceurl,
    image,
    servings,
    cookingTime,
    ingredients,
  }) {
    this.id = id;
    this.title = title;
    this.publisher = publisher;
    this.sourceUrl = sourceurl;
    this.image = image;
    this.servings = servings;
    this.cookingTime = cookingTime;
    this.ingredients = ingredients;
  }
}
