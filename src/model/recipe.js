export class Recipe {
  constructor({ id, title, publisher, image }) {
    this.id = id;
    this.title = title;
    this.publisher = publisher;
    this.image = image;
  }
}
