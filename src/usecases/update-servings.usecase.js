import { Detail } from "../model/detail";

export class UpdateServingsUseCase {
  static execute(recipe, newServings) {
    if (newServings < 1) {
      return recipe;
    }

    const updatedIngredients = recipe.ingredients.map((ingredient) => ({
      quantity: (ingredient.quantity * newServings) / recipe.servings,
      unit: ingredient.unit,
      description: ingredient.description,
    }));

    return new Detail({
      id: recipe.id,
      title: recipe.title,
      publisher: recipe.publisher,
      sourceurl: recipe.sourceurl,
      image: recipe.image,
      servings: newServings,
      cookingTime: recipe.cookingTime,
      ingredients: updatedIngredients,
    });
  }
}
