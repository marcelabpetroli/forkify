import { Detail } from "../model/detail";
import { RecipesRepository } from "../repositories/recipes.repository";

export class LoadRecipeUseCase {
  static async execute(id) {
    const repository = new RecipesRepository();
    const { data } = await repository.loadRecipe(id);
    const recipe = data.recipe;
    return new Detail({
      id: recipe.id,
      title: recipe.title,
      publisher: recipe.publisher,
      sourceUrl: recipe.source_url,
      image: recipe.image_url,
      servings: recipe.servings,
      cookingTime: recipe.cooking_time,
      ingredients: recipe.ingredients,
    });
  }
}
