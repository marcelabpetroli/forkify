import { Recipe } from "../model/recipe";
import { RecipesRepository } from "../repositories/recipes.repository";

export class SearchRecipesUseCase {
  static async execute(query) {
    const repository = new RecipesRepository();
    const { data } = await repository.searchRecipes(query);
    const recipes = data.recipes;
    return recipes.map(
      (recipe) =>
        new Recipe({
          id: recipe.id,
          title: recipe.title,
          publisher: recipe.publisher,
          image: recipe.image_url,
        })
    );
  }
}
