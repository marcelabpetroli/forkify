import { LitElement, html } from "lit";

export class CopyUI extends LitElement {
  render() {
    return html` <section>
      <p class="copyright">
        &copy; Copyright by
        <a
          rel="noopener"
          target="_blank"
          href="https://twitter.com/jonasschmedtman"
          >Jonas Schmedtmann</a
        >. Created and Designed for his JavaScript Udemy course
      </p>
    </section>`;
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("copy-ui", CopyUI);
