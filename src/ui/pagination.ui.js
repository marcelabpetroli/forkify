import { LitElement, html } from "lit";

export class PaginationUI extends LitElement {
  static get properties() {
    return {
      currentPage: { type: Number },
      totalPages: { type: Number },
    };
  }

  constructor() {
    super();
    this.currentPage = 1;
    this.totalPages = 0;
  }

  render() {
    const links = [];
    if (this.currentPage > 1) {
      links.push(html` <button
        class="page__btn btn--prev"
        @click="${() => this.setCurrentPage(this.currentPage - 1)}"
      >
        Page ${this.currentPage - 1}
      </button>`);
    }

    if (this.currentPage < this.totalPages) {
      links.push(html` <button
        class="page__btn btn--next"
        @click="${() => this.setCurrentPage(this.currentPage + 1)}"
      >
        Page ${this.currentPage + 1}
      </button>`);
    }
    return html`<div>${links}</div>`;
  }

  setCurrentPage(pageNumber) {
    this.currentPage = pageNumber;
    this.dispatchEvent(
      new CustomEvent("page:changed", {
        bubbles: true,
        detail: { pageNumber },
      })
    );
  }

  setTotalPages(totalPages) {
    this.totalPages = totalPages;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("pagination-ui", PaginationUI);
