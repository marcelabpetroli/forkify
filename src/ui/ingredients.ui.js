import { LitElement, html } from "lit";

export class IngredientsUI extends LitElement {
  static get properties() {
    return {
      ingredients: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <div class="recipe__ingredients">
        <h2 class="heading">Recipe ingredients</h2>
        <ul class="recipe__list">
          ${this.ingredients?.map(
            (ing) => html` <li class="recipe__item">
              <div class="recipe__quantity">
                ${ing.quantity === 0 ? "" : ing.quantity}
              </div>

              <div class="recipe__description">
                <span class="recipe__unit">${ing.unit}</span>
                ${ing.description}
              </div>
            </li>`
          )}
        </ul>
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("ingredients-ui", IngredientsUI);
