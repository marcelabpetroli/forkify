import { LitElement, html } from "lit";

export class HomeUI extends LitElement {
  render() {
    return html`
      <section class="go__home">
        <a class="link go__home--link" href="/"> Back to home </a>
      </section>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("home-ui", HomeUI);
