import { LitElement, html } from "lit";
import logo from "../assets/logo.png";

export class HeaderUI extends LitElement {
  render() {
    return html`
      <header>
        <a href="/">
          <img
            class="header__logo"
            src="${logo}"
            alt="Forkify logo"
            title="Forkify logo"
          />
        </a>
      </header>
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("header-ui", HeaderUI);
