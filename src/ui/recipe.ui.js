import { LitElement, html } from "lit";

export class RecipeUI extends LitElement {
  static get properties() {
    return {
      recipe: { type: Object },
    };
  }

  render() {
    return html`
      <a class="preview__link" href="#${this.recipe.id}">
        <figure class="preview__fig">
          <img
            src="${this.recipe.image}"
            alt="${this.recipe.title}"
            title="${this.recipe.title}"
          />
        </figure>
        <div class="preview__data">
          <h4 class="preview__title">${this.recipe.title}</h4>
          <p class="preview__publisher">${this.recipe.publisher}</p>
        </div>
      </a>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("recipe-ui", RecipeUI);
