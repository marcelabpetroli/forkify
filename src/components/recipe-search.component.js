import { LitElement, html } from "lit";

export class RecipeSearch extends LitElement {
  static get properties() {
    return {
      query: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.query = "";
  }

  render() {
    return html`
      <form class="search" @submit="${this.submitForm}">
        <input
          class="search__field"
          type="text"
          placeholder="Search for a recipe..."
          @change="${this.setQuery}"
          .value="${this.query}"
        />
        <button class="btn" type="submit">
          <span>Search</span>
        </button>
      </form>
    `;
  }

  setQuery(e) {
    this.query = e.target.value;
  }

  searchRecipe() {
    const query = this.query;

    this.dispatchEvent(
      new CustomEvent("search:recipe", {
        bubbles: true,
        detail: { query },
      })
    );
  }

  submitForm(e) {
    e.preventDefault();
    this.searchRecipe();
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("recipe-search", RecipeSearch);
