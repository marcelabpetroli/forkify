import { LitElement, html } from "lit";
import { LoadRecipeUseCase } from "../usecases/load-recipe.usecase";
import { UpdateServingsUseCase } from "../usecases/update-servings.usecase";
import { Detail } from "../model/detail";

export class RecipeDetailComponent extends LitElement {
  static get properties() {
    return {
      recipeDetail: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.recipeDetail = new Detail("", "", "", "", "", "", "", "");
  }

  render() {
    return html`
      ${!this.recipeDetail.id
        ? html`
            <div class="message">
              <p>Use the search bar to find thousands of inspiring recipes!</p>
            </div>
          `
        : html`
            <section class="recipe__box">
              <figure class="recipe__fig">
                <img
                  class="recipe__img"
                  src="${this.recipeDetail.image}"
                  alt="${this.recipeDetail.title}"
                />
              </figure>
              <h1 class="recipe__title">
                <span>${this.recipeDetail.title}</span>
              </h1>
            </section>

            <section class="recipe__details">
              <div class="recipe__info">
                <span class="recipe__info--data">
                  ${this.recipeDetail.cookingTime}</span
                >
                <span>minutes</span>
              </div>

              <div class="recipe__info">
                <span class="recipe__info--data"
                  >${this.recipeDetail.servings}</span
                >
                <span>servings</span>

                <div class="recipe__info--buttons">
                  <button
                    class="btn--servings btn--minus"
                    @click="${() =>
                      this.updateServings(this.recipeDetail.servings - 1)}"
                  >
                    -
                  </button>
                  <button
                    class="btn--servings"
                    @click="${() =>
                      this.updateServings(this.recipeDetail.servings + 1)}"
                  >
                    +
                  </button>
                </div>
              </div>
            </section>

            <ingredients-ui
              .ingredients="${this.recipeDetail.ingredients}"
            ></ingredients-ui>
          `}
    `;
  }

  async handleLoadRecipe(recipeId) {
    const recipeDetail = await LoadRecipeUseCase.execute(recipeId);

    this.recipeDetail.id = recipeDetail.id;
    this.recipeDetail.title = recipeDetail.title;
    this.recipeDetail.publisher = recipeDetail.publisher;
    this.recipeDetail.sourceUrl = recipeDetail.sourceurl;
    this.recipeDetail.image = recipeDetail.image;
    this.recipeDetail.servings = recipeDetail.servings;
    this.recipeDetail.cookingTime = recipeDetail.cookingTime;
    this.recipeDetail.ingredients = recipeDetail.ingredients;

    this.requestUpdate();
  }

  updateServings(newServings) {
    this.recipeDetail = UpdateServingsUseCase.execute(
      this.recipeDetail,
      newServings
    );
    this.requestUpdate();
  }

  reset() {
    this.recipeDetail = new Detail("", "", "", "", "", "", "", "");
    this.requestUpdate();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("recipe-detail", RecipeDetailComponent);
