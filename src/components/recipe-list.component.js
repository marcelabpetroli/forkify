import { LitElement, html } from "lit";
import { SearchRecipesUseCase } from "../usecases/search-recipes.usecase";

export class RecipeListComponent extends LitElement {
  static get properties() {
    return {
      results: { type: Array },
      currentPage: { type: Number },
      resultsPerPage: { type: Number },
      showNoResultsMessage: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.results = [];
    this.currentPage = 1;
    this.resultsPerPage = 10;
    this.showNoResultsMessage = false;
  }

  render() {
    const startIndex = (this.currentPage - 1) * this.resultsPerPage;
    const endIndex = startIndex + this.resultsPerPage;

    return html`
      ${this.showNoResultsMessage
        ? html`
            <div class="message">
              <p>There are no recipes that match your search. Try again! :)</p>
            </div>
          `
        : html`
            <ul class="results">
              ${this.results?.slice(startIndex, endIndex).map(
                (recipe) => html`
                  <li
                    class="preview"
                    @click="${this.selectRecipe}"
                    id="${recipe.id}"
                  >
                    <recipe-ui .recipe="${recipe}"></recipe-ui>
                  </li>
                `
              )}
            </ul>
            <pagination-ui
              class="pagination"
              .currentPage="${this.currentPage}"
              .totalPages="${Math.ceil(
                this.results.length / this.resultsPerPage
              )}"
            ></pagination-ui>
          `}
    `;
  }

  setCurrentPage(pageNumber) {
    this.currentPage = pageNumber;
    this.requestUpdate();
  }

  selectRecipe(e) {
    const clickedRecipe = this.results?.find(
      (recipe) => recipe.id === e.currentTarget.id
    );

    this.dispatchEvent(
      new CustomEvent("select:recipe", {
        bubbles: true,
        detail: {
          clickedRecipe: clickedRecipe,
        },
      })
    );
  }

  async handleRecipesResult(query) {
    this.currentPage = 1;
    const results = await SearchRecipesUseCase.execute(query);

    this.results = results;
    this.showNoResultsMessage = results.length === 0;
    this.requestUpdate();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("recipe-list", RecipeListComponent);
