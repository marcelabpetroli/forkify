export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
    .home__header {
      padding: 1rem;
    }
    
    .home {
      background-color: #fefefe;
      height: 88vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      gap: 10rem;
    }

    .home__title{
      font-size: 4rem;
      color: #615551;
    
      letter-spacing: 0.8rem;
    }
    .home__title--highlight {
      color: #f38e82;
    }
    
    .go__link {
      text-decoration: none;
    }

    .link--go {
      cursor: pointer;
    }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = `
     <header-ui class="home__header"></header-ui>
    
     <main class="home">
       <h1 class="home__title">Welcome to 
       <span class="home__title--highlight"> Forkify!</span> 
       Ready to get inspired?
       </h1>
       <a class="go__link link link--go" href="/recipes">
       Let's go!
       </a>
     </main>
   
     ${this.getStyles()}
    `;
  }
}

customElements.define("home-page", HomePage);
