export class RecipesPage extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
        <main class="container">
          <header-ui class="header"></header-ui>
          <recipe-search></recipe-search>
          <recipe-list class="search__results" id="recipe__list"></recipe-list>
          <recipe-detail class="recipe" id="recipe__detail"></recipe-detail>
          <copy-ui></copy-ui>
          <home-ui></home-ui>
        </main>
          `;

    const listComponent = this.querySelector("#recipe__list");
    const detailComponent = this.querySelector("#recipe__detail");

    this.addEventListener("select:recipe", (e) => {
      detailComponent.handleLoadRecipe(e.detail.clickedRecipe.id);
    });

    this.addEventListener("search:recipe", (e) => {
      listComponent.handleRecipesResult(e.detail.query);
      detailComponent.reset();
    });

    this.addEventListener("page:changed", (e) => {
      listComponent.setCurrentPage(e.detail.pageNumber);
    });
  }
}

customElements.define("recipes-page", RecipesPage);
